package cn.net.scp.backperssure;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private static final AtomicInteger finishedTasks = new AtomicInteger();
    private static final AtomicInteger failedTasks = new AtomicInteger();
    private static final AtomicInteger interruptedTasks = new AtomicInteger();

    public static void main(String[] args) {
        final int N_TASKS = 100;
        final Broker broker = new Broker();
        final List<CompletableFuture<Result>> completableFutures = new ArrayList<>();
        for (int i = 0; i < N_TASKS; i++) {
            final Task task = createCalculationTask(i);
            final CompletableFuture<Result> calculationResultCompletableFuture = broker.submit(task);
            completableFutures.add(calculationResultCompletableFuture);
            if (i == 30) {
                sleep(3000);
            }
            if (i % 3 == 0) {
                final Task taskCopy = createCalculationTask(i);
                final CompletableFuture<Result> calculationResultCompletableFutureCopy = broker.submit(taskCopy);
                completableFutures.add(calculationResultCompletableFutureCopy);
            }
        }

        broker.close();
        sleep();
        completableFutures.forEach(Main::completeFuture);
        LOGGER.info("Total tasks submitted: {}", completableFutures.size());
        LOGGER.info("Total tasks finished: {}.", finishedTasks.get());
        LOGGER.info("Failed tasks: {}.", failedTasks.get());
        LOGGER.info("Interrupted tasks: {}.", interruptedTasks.get());
    }

    private static Task createCalculationTask(final int counter) {
        return new Task("CalculationTask_" + counter);
    }

    private static void sleep(final int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException("Failed to sleep.", e);
        }
    }

    private static void sleep() {
        sleep(10000);
    }

    private static void completeFuture(final CompletableFuture<Result> future) {
        final Result result;
        try {
            result = future.get();
            LOGGER.info("Task is finished: {}.", result);
            finishedTasks.incrementAndGet();
        } catch (InterruptedException e) {
            LOGGER.error("Task was interrupted: {}.", e.getMessage());
            interruptedTasks.incrementAndGet();
        } catch (ExecutionException e) {
            LOGGER.error("Task failed.");
            failedTasks.incrementAndGet();
        }
    }
}
